class AndroidParser {
  constructor(ua) {
    this.ua = ua;
    this.device={
      huawei:["","",""],
      mi:[],
      oppo:[],
      vivo:[],
      google:[],
      meizu:[],
      samsung:[],
      zte:[]
    }
  }
  /**
   * 
   */
  getDeviceType(){
    if(this.ua.indexOf("build")>-1){
      let deviceType=this.ua.match(/; (\S*) build/)?this.ua.match(/; (\S*) build/)[1]:null;
      if(deviceType){
            for (let key in this.device) {
               let list=this.device[key];
               let result=this.device[key].filter((item)=>{return item.indexOf(deviceType)>-1;})
               if(result.length>0){
                 return key;
               }
            }
            return deviceType;
      }else{
        return deviceType;
      }
    }
  }
}

export default AndroidParser